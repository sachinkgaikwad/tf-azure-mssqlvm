resource "azurerm_storage_account" "mssql_storage" {
  name                = "${var.customer}${var.azure_location_short}mssqlstorage"
  resource_group_name = "${azurerm_resource_group.mssql_group.name}"
  location            = "${var.azure_location}"
  account_type        = "Premium_LRS"

  tags {
    environment = "${var.envtype}"
  }
}

resource "azurerm_storage_container" "mssql_backups_container" {
  name                  = "${var.customer}mssqlbackups"
  resource_group_name   = "${azurerm_resource_group.mssql_group.name}"
  storage_account_name  = "${azurerm_storage_account.mssql_storage.name}"
  container_access_type = "private"
}

/* Environment variables */
variable "customer" {
  description = "The first portion of the interpolated name for resources created by this module"
  type = "string"
  default = "cust"
}

variable "envtype" {
  description = "The first portion of the interpolated name for resources created by this module"
  type = "string"
  default = "dev"
}

variable "network_remote_state_access_key" {
  description = "The remote state bucket access key"
  type = "string"
}

variable "azure_location" {
  description = "The region in which to create resources"
  type = "string"
  default = "North Europe"
}

variable "azure_location_short" {
  description = "The short name for the region in which to create resources"
  type = "string"
  default = "ne"
}

variable "subnet_index" {
  description = "This is a manual index used when selecting the subnet from the remote state"
  type = "string"
}

/* Instance settings */
variable "instance_number" {
  description = "Count of how many instances this module should create"
  type = "string"
  default = 1
}

variable "local_adminuser" {
  description = "The desired username for the local adminstrator account"
  type = "string"
}

variable "local_adminpassword" {
  description = "The desired password for the local adminstrator account"
  type = "string"
}

/* Image settings */
variable "offer" {
  description = "Specifies the offer of the image used to create the virtual machine"
  type = "string"
  default = "SQL2014SP2-WS2012R2"
}

variable "sku" {
  description = "Specifies the SKU of the image used to create the virtual machine"
  type = "string"
  default = "Enterprise"
}

variable "app_disk_size" {
  description = "Specifies the size of the os disk in gigabytes"
  type = "string"
  default = "128"
}

variable "data_disk_size" {
  description = "Specifies the size of the data disk in gigabytes"
  type = "string"
  default = "128"
}

variable "log_disk_size" {
  description = "Specifies the size of the log disk in gigabytes"
  type = "string"
  default = "128"
}

variable "tempdb_disk_size" {
  description = "Specifies the size of the tempdb disk in gigabytes"
  type = "string"
  default = "128"
}

variable "dbindex_disk_size" {
  description = "Specifies the size of the indexing disk in gigabytes"
  type = "string"
  default = "128"
}

/* Tags */

## Licence not actually used anywhere ? -jg
variable "Licence" {
  default = "Included"
}
##

variable "tier" {
  description = "The value of the 'Service' tag"
  type = "string"
  default = "db"
}

/* Active Directory Domain variables */
variable "ad_domain" {
  description = "The FQDN of the Active Directory domain to join"
  type = "string"
}

variable "domainadmin_username" {
  description = "The username of an account with join rights to the domain"
  type = "string"
}

variable "domainadmin_password" {
  description = "The password of the account specified in 'domainadmin_username'"
  type = "string"
}

/* MSSQL variables */
variable "sql_adminuser" {
  description = "The username for the SQL root administrative account"
  type = "string"
}

variable "sql_adminpassword" {
  description = "The password for the account specified in 'sql_adminuser'"
  type = "string"
}

resource "azurerm_resource_group" "mssql_group" {
  name     = "${var.customer}-${var.envtype}-${var.azure_location}-mssql"
  location = "${var.azure_location}"

  tags {
    environment = "${var.envtype}"
  }
}

resource "azurerm_availability_set" "MSSQL-AvailabilitySet" {
  name                = "${var.customer}-${var.envtype}-${var.azure_location}-MSSQL-ASet"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.mssql_group.name}"
  managed             = "true"

  tags {
    environment = "${var.envtype}"
    service     = "${var.tier}"
  }
}

resource "azurerm_network_interface" "mssql_network_interface" {
  count               = "${var.instance_number}"
  name                = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-interface"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.mssql_group.name}"

  //network_security_group_id = "${azurerm_network_security_group.windows_bastion_sg.id}"

  ip_configuration {
    name                          = "${var.customer}-${var.envtype}--${var.azure_location}-mssql-${count.index}-ipconfig"
    subnet_id                     = "${element(split(",",data.terraform_remote_state.network.nat_subnets),var.subnet_index)}"
    private_ip_address_allocation = "dynamic"
  }
}

data "template_file" "disk_config" {
  template = "${file("${path.module}/disk_config.tmpl")}"
}

data "template_file" "first_logon" {
  template = "${file("${path.module}/firstlogoncommands.tmpl")}"
}

resource "azurerm_virtual_machine" "mssql_vm" {
  count                 = "${var.instance_number}"
  name                  = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}"
  location              = "${var.azure_location}"
  resource_group_name   = "${azurerm_resource_group.mssql_group.name}"
  network_interface_ids = ["${element(azurerm_network_interface.mssql_network_interface.*.id, count.index)}"]
  vm_size               = "Standard_DS3_v2"
  availability_set_id   = "${azurerm_availability_set.MSSQL-AvailabilitySet.id}"

  storage_image_reference {
    publisher = "MicrosoftSQLServer"
    offer     = "${var.offer}"
    sku       = "${var.sku}"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-OS.vhd"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${var.customer}-${var.envtype}--${var.azure_location}-mssql-${count.index}-app.vhd"
    disk_size_gb      = "${var.app_disk_size}"
    create_option     = "empty"
    lun               = 0
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-data.vhd"
    disk_size_gb      = "${var.data_disk_size}"
    create_option     = "empty"
    lun               = 1
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-logs.vhd"
    disk_size_gb      = "${var.log_disk_size}"
    create_option     = "empty"
    lun               = 2
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-tempdb.vhd"
    disk_size_gb      = "${var.tempdb_disk_size}"
    create_option     = "empty"
    lun               = 3
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-dbindex.vhd"
    disk_size_gb      = "${var.dbindex_disk_size}"
    create_option     = "empty"
    lun               = 4
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.azure_location_short}-mssql-${count.index}"
    admin_username = "${var.local_adminuser}"
    admin_password = "${var.local_adminpassword}"

    #Include Deploy.PS1 with variables injected as custom_data
    custom_data = "${base64encode(data.template_file.disk_config.rendered)}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true

    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.local_adminpassword}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.local_adminuser}</Username></AutoLogon>"
    }

    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = "${data.template_file.first_logon.rendered}"
    }
  }
}

resource "azurerm_virtual_machine_extension" "mssql_dcjoin" {
  count                = "${var.instance_number}"
  name                 = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}-dcjoin"
  location             = "${var.azure_location}"
  depends_on           = ["azurerm_virtual_machine.mssql_vm"]
  resource_group_name  = "${azurerm_resource_group.mssql_group.name}"
  virtual_machine_name = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}"
  publisher            = "Microsoft.Compute"
  type                 = "JsonADDomainExtension"
  type_handler_version = "1.0"

  settings = <<SETTINGS
  {
    "Name": "${var.ad_domain}",
    "User": "${var.ad_domain}\\${var.domainadmin_username}",
    "Restart": "true",
    "Options" :  "3"
}
SETTINGS

  protected_settings = <<SETTINGS
{
                "Password":"${var.domainadmin_password}"
                }
SETTINGS
}

resource "azurerm_virtual_machine_extension" "Malware_Protection_Extension" {
  count                = "${var.instance_number}"
  name                 = "IaaSAntimalware"
  location             = "${var.azure_location}"
  depends_on           = ["azurerm_virtual_machine.mssql_vm"]
  resource_group_name  = "${azurerm_resource_group.mssql_group.name}"
  virtual_machine_name = "${var.customer}-${var.envtype}-${var.azure_location}-mssql-${count.index}"
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.5"

  settings = <<SETTINGS
    { "AntimalwareEnabled": true, 
    "RealtimeProtectionEnabled": true, 
    "ScheduledScanSettings": { 
        "isEnabled": true, 
        "day": "1", 
        "time": "120", 
        "scanType": "Full" }, 
        "Exclusions": { 
            "Extensions": ".mdf;.ldf", 
            "Paths": "c:\\windows;c:\\windows\\system32", 
            "Processes": "taskmgr.exe;notepad.exe" } 
            }
SETTINGS
}
